FIND_PATH(LAMMPS_INCLUDE_DIR lammps.h HINTS
  ${LAMMPS_PREFIX}/src
  /usr/include
  /usr/local/include
  /opt/local/include
  /sw/include
)
FIND_LIBRARY(LAMMPS_LIBRARY NAMES lammps HINTS
  ${LAMMPS_PREFIX}/src
  /usr/lib
  /usr/local/lib
  /opt/local/lib
  /sw/lib
)

