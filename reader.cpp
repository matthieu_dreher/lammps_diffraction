#include <decaf/decaf.hpp>
#include <decaf/data_model/simplefield.hpp>
#include <decaf/data_model/arrayfield.hpp>
#include <decaf/data_model/boost_macros.h>

#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <map>
#include <cstdlib>


using namespace decaf;
using namespace std;

// gets the atom positions and prints them
void print(Decaf* decaf)
{
    vector< pConstructData > in_data;

    while (decaf->get(in_data))
    {
        // get the values
        for (size_t i = 0; i < in_data.size(); i++)
        {
            ArrayFieldd pos = in_data[i]->getFieldData<ArrayFieldd>("pos");
            ArrayFieldi types = in_data[i]->getFieldData<ArrayFieldi>("types");
            if (pos && types)
            {
                // debug
                fprintf(stderr, "consumer print1 or print3 printing %d atoms\n",
                        pos.getNbItems());
                double* array = pos.getArray();
                int* array_types = types.getArray();
                for (int i = 0; i < 10; i++)               // print first few atoms
                    fprintf(stderr, "type: %i, pos: %.3lf %.3lf %.3lf\n",
                    array_types[i], array[3 * i], array[3 * i + 1], array[3 * i + 2]);
            }
            else
                fprintf(stderr, "Error: null pointer in node2\n");
        }
    }

    // terminate the task (mandatory) by sending a quit message to the rest of the workflow
    fprintf(stderr, "print terminating\n");
    decaf->terminate();
}


int main(int argc,
     char** argv)
{
    // define the workflow
    Workflow workflow;
    Workflow::make_wflow_from_json(workflow, "lammps_diff.json");

    MPI_Init(NULL, NULL);

    // create decaf
    Decaf* decaf = new Decaf(MPI_COMM_WORLD, workflow);

    // Running the loop
    print(decaf);

    // cleanup
    delete decaf;
    MPI_Finalize();

    return 0;
}
