#! /bin/bash

CXXFLAGS="-DTRANSPORT_MPI -std=c++11" CPPFLAGS="-I/home/matthieu/Argonne/Decaf/source_froggy/install/include -I/home/matthieu/Dependencies/include" LDFLAGS="-L/home/matthieu/Argonne/Decaf/source_froggy/install/lib" LIBS="-ldca -lbca" ./configure --prefix=/home/matthieu/Argonne/Decaf/source_froggy/decaf_app/lammps_diffraction/install --without-bzlib --enable-mpi
