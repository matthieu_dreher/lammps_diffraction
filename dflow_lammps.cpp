#include <decaf/decaf.hpp>
#include <decaf/data_model/simplefield.hpp>
#include <decaf/data_model/arrayfield.hpp>
#include <decaf/data_model/boost_macros.h>

#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <map>
#include <cstdlib>


using namespace decaf;
using namespace std;

// link callback function
extern "C"
{

    // dataflow between tess and dense for now just forwards everything
    void dflow_lammps(void* args,                   // arguments to the callback
                Dataflow* dataflow,                 // dataflow
                pConstructData in_data)             // input data
    {
        dataflow->put(in_data, DECAF_LINK);
    }

} // extern "C"

int main(int argc,
     char** argv)
{
    // define the workflow
    Workflow workflow;
    Workflow::make_wflow_from_json(workflow, "lammps_diff.json");

    MPI_Init(NULL, NULL);

    // create decaf
    Decaf* decaf = new Decaf(MPI_COMM_WORLD, workflow);

    // cleanup
    delete decaf;
    MPI_Finalize();

    return 0;
}
