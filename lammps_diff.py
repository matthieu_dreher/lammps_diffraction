# a small 2-node example, just a producer and consumer

# --- include the following 4 lines each time ---

import networkx as nx
import os
import imp
wf = imp.load_source('workflow', os.environ['DECAF_PREFIX'] + '/python/workflow.py')

# --- set your options here ---

# path to .so module for dataflow callback functions
mod_path = '/home/matthieu/Argonne/Decaf/source_froggy/decaf_app/lammps_diffraction/install/bin/mod_lammps.so'

# define workflow graph
# 2-node workflow
#
#    prod (4 procs) -> con (2 procs)
#
#  entire workflow takes 8 procs (2 dataflow procs between producer and consumer)
#  dataflow can be overlapped, but currently all disjoint procs (simplest case)

w = nx.DiGraph()
w.add_node("lammps", start_proc=0, nprocs=4, func='lammps', cmdline='./bin/lammps_prod in.eam')

#w.add_node("reader",  start_proc=6, nprocs=2, func='reader', cmdline='./bin/reader')
#w.add_edge("lammps", "reader", start_proc=4, nprocs=2, func='dflow_lammps', path=mod_path,
#           prod_dflow_redist='proc', dflow_con_redist='proc', cmdline='./bin/dflow_lammps')


w.add_node("debyer",  start_proc=6, nprocs=2, func='debyer', cmdline='./bin/debyer -x -f37 -t39 -s0.01 -l1.5418 -a0 -b0 -c0 -r600 --save-id=IAD_1.dat -o DP_1.dat in.decaf')
w.add_edge("lammps", "debyer", start_proc=4, nprocs=2, func='dflow_lammps', path=mod_path,
           prod_dflow_redist='proc', dflow_con_redist='proc', cmdline='./bin/dflow_lammps')
# --- convert the nx graph into a workflow data structure and run the workflow ---

wf.processGraph(w, "lammps_diff")
