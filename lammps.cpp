//---------------------------------------------------------------------------
//
// lammps example
//
// 4-node workflow
//
//          print (1 proc)
//        /
//    lammps (4 procs)
//        \
//          print2 (1 proc) - print (1 proc)
//
//  entire workflow takes 10 procs (1 dataflow proc between each producer consumer pair)
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// tpeterka@mcs.anl.gov
//
//--------------------------------------------------------------------------
#include <decaf/decaf.hpp>
#include <decaf/data_model/pconstructtype.h>
#include <decaf/data_model/arrayfield.hpp>
#include <decaf/data_model/boost_macros.h>

#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <string.h>
#include <utility>
#include <map>

// lammps includes
#include "lammps.h"
#include "input.h"
#include "atom.h"
#include "domain.h"
#include "library.h"

using namespace decaf;
using namespace LAMMPS_NS;
using namespace std;

struct lammps_args_t                         // custom args for running lammps
{
    LAMMPS* lammps;
    string infile;
};

struct pos_args_t                            // custom args for atom positions
{
    int natoms;                              // number of atoms
    double* pos;                             // atom positions
};

// runs lammps and puts the atom positions to the dataflow at the consumer intervals
void lammps(Decaf* decaf, int nsteps, int analysis_interval, string infile)
{
    // LAMMPS(int narg, char **arg, MPI_Comm communicator)
    LAMMPS* lps = new LAMMPS(0, NULL, decaf->local_comm_handle());
    //LAMMPS* lps = new LAMMPS(0, NULL, MPI_COMM_WORLD);
    lps->input->file(infile.c_str());

    int natoms = lammps_get_natoms(lps);
    double* x = new double[3 * natoms];
    bzero(x, 3 * natoms * sizeof(double));
    int* types = new int[natoms];
    bzero(types, natoms * sizeof(int));
    double* H = new double[9];
    bzero(H, 9 * sizeof(double));

    //Setting up the the bounding box
    H[0] = lps->domain->xprd;
    H[1] = 0.0;
    H[2] = 0.0;
    H[3] = lps->domain->xy;
    H[4] = lps->domain->yprd;
    H[5] = 0.0;
    H[6] = lps->domain->xz;
    H[7] = lps->domain->yz;
    H[8] = lps->domain->zprd;

    // First outputing the final state of the simulation
    lammps_gather_atoms(lps, (char*)"x", 1, 3, x);
    lammps_gather_atoms(lps, (char*)"type", 0, 1, types);

    pConstructData container;

    for (int i = 0; i < 10; i++)         // print first few atoms
        fprintf(stderr, "type: %i, pos: %.3lf %.3lf %.3lf\n",
        types[i], x[3 * i], x[3 * i + 1], x[3 * i + 2]);


    ArrayFieldd data(x, 3 * natoms, 3, false);
    ArrayFieldi typesField(types, natoms, 1, false);
    ArrayFieldd bbox(H, 9, 1, false);

    container->appendData("pos", data,
        DECAF_NOFLAG, DECAF_PRIVATE,
        DECAF_SPLIT_DEFAULT, DECAF_MERGE_FIRST_VALUE);
    container->appendData("types", typesField,
        DECAF_NOFLAG, DECAF_PRIVATE,
        DECAF_SPLIT_DEFAULT, DECAF_MERGE_FIRST_VALUE);
    container->appendData("H", bbox,
        DECAF_NOFLAG, DECAF_SHARED,
        DECAF_SPLIT_DEFAULT, DECAF_MERGE_FIRST_VALUE);

    decaf->put(container);

    // Advancing the simulation
    for (int timestep = 0; timestep < nsteps; timestep++)
    {
        fprintf(stderr, "lammps\n");

        lps->input->one("run 1");

        /* ----------------------------------------------------------------------
           gather the named atom-based entity across all processors
           atom IDs must be consecutive from 1 to N
           name = desired quantity, e.g. x or charge
           type = 0 for integer values, 1 for double values
           count = # of per-atom values, e.g. 1 for type or charge, 3 for x or f
           return atom-based values in data, ordered by count, then by atom ID
             e.g. x[0][0],x[0][1],x[0][2],x[1][0],x[1][1],x[1][2],x[2][0],...
             data must be pre-allocated by caller to correct length
        ------------------------------------------------------------------------- */

        lammps_gather_atoms(lps, (char*)"x", 1, 3, x);
        lammps_gather_atoms(lps, (char*)"type", 0, 1, types);

        if (!((timestep + 1) % analysis_interval))
        {
            if (decaf->local_comm_rank() == 0)
            {
                /*double xs,ys,zs;
                double boxxlo = lps->domain->boxlo[0];
                double boxylo = lps->domain->boxlo[1];
                double boxzlo = lps->domain->boxlo[2];
                double invxprd = 1.0/lps->domain->xprd;
                double invyprd = 1.0/lps->domain->yprd;
                double invzprd = 1.0/lps->domain->zprd;
                for(int i = 0; i < 10; i++)
                {
                    xs = (x[3*i] - boxxlo) * invxprd;
                    ys = (x[3*i+1] - boxylo) * invyprd;
                    zs = (x[3*i+2] - boxzlo) * invzprd;
                    fprintf(stderr, "Atom %i: %lf %lf %lf\n",i, xs,ys,zs);
                    xs = (lps->atom->x[i][0] - boxxlo) * invxprd;
                    ys = (lps->atom->x[i][1] - boxylo) * invyprd;
                    zs = (lps->atom->x[i][2] - boxzlo) * invzprd;
                    fprintf(stderr, "Atom %i: %lf %lf %lf\n",i, xs,ys,zs);
                }*/

                fprintf(stderr,"H0(1,1) = %g A\n",lps->domain->xprd);
                fprintf(stderr,"H0(1,2) = 0 A \n");
                fprintf(stderr,"H0(1,3) = 0 A \n");
                fprintf(stderr,"H0(2,1) = %g A \n",lps->domain->xy);
                fprintf(stderr,"H0(2,2) = %g A\n",lps->domain->yprd);
                fprintf(stderr,"H0(2,3) = 0 A \n");
                fprintf(stderr,"H0(3,1) = %g A \n",lps->domain->xz);
                fprintf(stderr,"H0(3,2) = %g A \n",lps->domain->yz);
                fprintf(stderr,"H0(3,3) = %g A\n",lps->domain->zprd);


            }

            pConstructData container;

            fprintf(stderr, "lammps producing time step %d with %d atoms\n",
                    timestep, natoms);

            for (int i = 0; i < 10; i++)         // print first few atoms
                fprintf(stderr, "type: %i, pos: %.3lf %.3lf %.3lf\n",
                types[i], x[3 * i], x[3 * i + 1], x[3 * i + 2]);


            ArrayFieldd data(x, 3 * natoms, 3, false);
            ArrayFieldi typesField(types, natoms, 1, false);
            ArrayFieldd bbox(H, 9, 1, false);

            container->appendData("pos", data,
                DECAF_NOFLAG, DECAF_PRIVATE,
                DECAF_SPLIT_DEFAULT, DECAF_MERGE_FIRST_VALUE);
            container->appendData("types", typesField,
                DECAF_NOFLAG, DECAF_PRIVATE,
                DECAF_SPLIT_DEFAULT, DECAF_MERGE_FIRST_VALUE);
            container->appendData("H", bbox,
                DECAF_NOFLAG, DECAF_SHARED,
                DECAF_SPLIT_DEFAULT, DECAF_MERGE_FIRST_VALUE);

            decaf->put(container);
        }
    }

    // terminate the task (mandatory) by sending a quit message to the rest of the workflow
    fprintf(stderr, "lammps terminating\n");
    decaf->terminate();
    fprintf(stderr, "Terminate signal sent\n");
    delete [] x;
    delete [] types;
    delete [] H;

    MPI_Barrier(decaf->local_comm_handle());
    delete lps;
}


// test driver for debugging purposes
// normal entry point is run(), called by python
int main(int argc,
         char** argv)
{
    int lammps_nsteps     = 1;
    int analysis_interval = 1;

    if(argc != 2)
    {
        fprintf(stderr, "Usage: lammps_prod infile\n");
        MPI_Abort(MPI_COMM_WORLD, 0);
    }

    // define the workflow
    Workflow workflow;
    Workflow::make_wflow_from_json(workflow, "lammps_diff.json");

    MPI_Init(NULL, NULL);
    Decaf* decaf = new Decaf(MPI_COMM_WORLD, workflow);

    string infile = argv[1];
    lammps(decaf, lammps_nsteps, analysis_interval, infile);

    // cleanup
    delete decaf;
    MPI_Finalize();

    return 0;
}
